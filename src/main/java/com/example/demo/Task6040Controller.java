package com.example.demo;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Task6040Controller {
	@CrossOrigin
	@GetMapping("/listEmployee")
	public ArrayList<Employee> getEmployeeList() {
		ArrayList<Employee> employeeList = new ArrayList<Employee>();
		
		BasePlusCommisionEmployee myEmployee1 = new BasePlusCommisionEmployee("Peter", "Pan", "abc123", 400000000, 0.2 , 20000000);
		HourlyEmployee myEmployee2 = new HourlyEmployee("Ran", "Mori", "cde321", 50000, 240);
		SalariedEmployee myEmployee3 = new SalariedEmployee("Logan", "Nguyen", "efk678", 4000000);
		
		employeeList.add(myEmployee1);
		employeeList.add(myEmployee2);
		employeeList.add(myEmployee3);
		
		
		
		return employeeList;
	}
	
	@CrossOrigin
	@GetMapping("/listInvoice")
	public ArrayList<Invoice> getInvoiceList() {
		ArrayList<Invoice> invoiceList = new ArrayList<Invoice>();
		
		Invoice myInvoice1 = new Invoice("ELEC123", "Electric Invoice", 150, 3500);
		Invoice myInvoice2 = new Invoice("WAT123", "Water Invoice", 50, 3500);
		Invoice myInvoice3 = new Invoice("TAX123", "Tax Invoice", 1, 6000000);
		
		invoiceList.add(myInvoice1);
		invoiceList.add(myInvoice2);
		invoiceList.add(myInvoice3);
		
		return invoiceList;
	}
}
