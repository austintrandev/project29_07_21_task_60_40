package com.example.demo;

public interface Payable {
public double getPaymentAmount();
}
