package com.example.demo;

public class BasePlusCommisionEmployee extends CommissionEmployee {
	public BasePlusCommisionEmployee(String firstName, String lastName, String socialSecurityNumber, double grossSales,
			double commisionRate, double baseSalary) {
		super(firstName, lastName, socialSecurityNumber, grossSales, commisionRate);
		this.baseSalary = baseSalary;
	}

	private double baseSalary;

	public double getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(double baseSalary) {
		this.baseSalary = baseSalary;
	}

	@Override
	public double getPaymentAmount() {
		return (grossSales + grossSales * commisionRate) + baseSalary;
	}
}
